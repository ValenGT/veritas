from typing import Text
from rest_framework import views
from rest_framework.response import Response
from serializers.comentarioSerializer import ComentarioSerializer, ReadCommentSerializer
from models.comentario import Comentario

class ComentarioView(views.APIView):
    def get(self, request):
        listar_comentarios = Comentario.objects.all()
        serializer = ComentarioSerializer(listar_comentarios, many=True)
        return Response(serializer.data, 200)

    def post(self, request):
        datos_json = request.data
        serializer = ReadCommentSerializer(data=datos_json)
        if serializer.is_valid():
            serializer.save()
            return Response({"mensaje": "Comentario creado"}, 200)
        else:
            return Response(serializer.errors, 405)

    def delete(self, request, pk):
        try:
            comment = Comentario.objects.get(pk=pk)
            comment.delete()
            return Response({"mensaje": "comentario borrado"}, 200)
        except:
            return Response({"mensaje": "comentario no existe, ingrese pk valido"}, 400)

    def update(self, request,pk):

        datos_json = request.data
        serializer = ReadCommentSerializer(data=datos_json)
        if serializer.is_valid():
            queryset = Comentario.objects.get(pk=pk) #no estoy segura de como concordar linea
            serializer.save(C)
            return Response({"mensaje": "Comentario creado"}, 200)
        else:
            return Response(serializer.errors, 405)

        '''queryset = Comentario.objects.get(pk=pk)
        serializer_class = ComentarioSerializer.ComentarioSerializer
        self.date_created = Comentario.texto()
        return super('' , self).save()'''

   
